"use strict";
const common_vendor = require("../../../../common/vendor.js");
const common_assets = require("../../../../common/assets.js");
const _sfc_main = {
  name: "ccPickerView",
  props: {
    // 左边标题栏
    leftTitle: {
      type: String,
      default: ""
    },
    // 默认输入占位符
    placeholder: {
      type: String,
      default: ""
    },
    // 输入框name
    name: {
      type: String,
      default: ""
    },
    // 当前index
    value: {
      type: [Number, String],
      default: 0
    },
    // 数据源
    range: {
      type: Array,
      default() {
        return [];
      }
    }
  },
  data() {
    return {};
  },
  methods: {
    changeClick(e) {
      this.$emit("change", e);
    }
  }
};
if (!Array) {
  const _easycom_uni_col2 = common_vendor.resolveComponent("uni-col");
  const _easycom_uni_row2 = common_vendor.resolveComponent("uni-row");
  (_easycom_uni_col2 + _easycom_uni_row2)();
}
const _easycom_uni_col = () => "../../../uni-row/components/uni-col/uni-col.js";
const _easycom_uni_row = () => "../../../uni-row/components/uni-row/uni-row.js";
if (!Math) {
  (_easycom_uni_col + _easycom_uni_row)();
}
function _sfc_render(_ctx, _cache, $props, $setup, $data, $options) {
  return {
    a: common_vendor.t($props.leftTitle),
    b: common_vendor.p({
      span: 8
    }),
    c: common_vendor.o((...args) => $options.changeClick && $options.changeClick(...args)),
    d: $props.value,
    e: $props.range,
    f: common_vendor.p({
      span: 12
    }),
    g: $props.name,
    h: $props.placeholder,
    i: $props.range[$props.value],
    j: common_vendor.o(($event) => $props.range[$props.value] = $event.detail.value),
    k: common_vendor.p({
      span: 2
    }),
    l: common_assets._imports_0$2,
    m: common_vendor.p({
      span: 2
    })
  };
}
const Component = /* @__PURE__ */ common_vendor._export_sfc(_sfc_main, [["render", _sfc_render]]);
wx.createComponent(Component);
