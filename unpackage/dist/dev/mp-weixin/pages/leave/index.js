"use strict";
const common_vendor = require("../../common/vendor.js");
const common_assets = require("../../common/assets.js");
if (!Array) {
  const _easycom_uni_col2 = common_vendor.resolveComponent("uni-col");
  const _easycom_uni_row2 = common_vendor.resolveComponent("uni-row");
  (_easycom_uni_col2 + _easycom_uni_row2)();
}
const _easycom_uni_col = () => "../../uni_modules/uni-row/components/uni-col/uni-col.js";
const _easycom_uni_row = () => "../../uni_modules/uni-row/components/uni-row/uni-row.js";
if (!Math) {
  (_easycom_uni_col + _easycom_uni_row)();
}
const _sfc_main = {
  __name: "index",
  setup(__props) {
    let toApply = function() {
      common_vendor.index.navigateTo({
        url: "/pages/leave/apply"
      });
    };
    return (_ctx, _cache) => {
      return {
        a: common_assets._imports_0,
        b: common_vendor.o((...args) => common_vendor.unref(toApply) && common_vendor.unref(toApply)(...args)),
        c: common_vendor.p({
          span: 20,
          offset: 2
        })
      };
    };
  }
};
wx.createPage(_sfc_main);
