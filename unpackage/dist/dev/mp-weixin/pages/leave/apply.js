"use strict";
const common_vendor = require("../../common/vendor.js");
const common_assets = require("../../common/assets.js");
if (!Array) {
  const _easycom_cc_pickerView2 = common_vendor.resolveComponent("cc-pickerView");
  const _easycom_uni_card2 = common_vendor.resolveComponent("uni-card");
  const _easycom_uni_col2 = common_vendor.resolveComponent("uni-col");
  const _easycom_uni_row2 = common_vendor.resolveComponent("uni-row");
  (_easycom_cc_pickerView2 + _easycom_uni_card2 + _easycom_uni_col2 + _easycom_uni_row2)();
}
const _easycom_cc_pickerView = () => "../../uni_modules/cc-pickerView/components/cc-pickerView/cc-pickerView.js";
const _easycom_uni_card = () => "../../uni_modules/uni-card/components/uni-card/uni-card.js";
const _easycom_uni_col = () => "../../uni_modules/uni-row/components/uni-col/uni-col.js";
const _easycom_uni_row = () => "../../uni_modules/uni-row/components/uni-row/uni-row.js";
if (!Math) {
  (_easycom_cc_pickerView + _easycom_uni_card + _easycom_uni_col + _easycom_uni_row)();
}
const _sfc_main = {
  __name: "apply",
  setup(__props) {
    let value = common_vendor.ref(1);
    let range = common_vendor.ref(["东", "南", "西", "北", "南北"]);
    let towardPickerChange = function(e) {
      this.value = e.target.value;
    };
    return (_ctx, _cache) => {
      return {
        a: common_vendor.o(common_vendor.unref(towardPickerChange)),
        b: common_vendor.p({
          leftTitle: "是否离开武汉",
          name: "orientations",
          placeholder: "请选择房屋朝向",
          value: common_vendor.unref(value),
          range: common_vendor.unref(range)
        }),
        c: common_vendor.o(common_vendor.unref(towardPickerChange)),
        d: common_vendor.p({
          leftTitle: "请假类型",
          name: "orientations",
          placeholder: "请选择房屋朝向",
          value: common_vendor.unref(value),
          range: common_vendor.unref(range)
        }),
        e: common_vendor.p({
          span: 19
        }),
        f: common_assets._imports_0$1,
        g: common_vendor.p({
          span: 5
        }),
        h: common_assets._imports_1,
        i: common_vendor.o(common_vendor.unref(towardPickerChange)),
        j: common_vendor.p({
          leftTitle: "请假类型",
          name: "orientations",
          placeholder: "请选择房屋朝向",
          value: common_vendor.unref(value),
          range: common_vendor.unref(range)
        })
      };
    };
  }
};
const MiniProgramPage = /* @__PURE__ */ common_vendor._export_sfc(_sfc_main, [["__scopeId", "data-v-2d3d0cab"]]);
wx.createPage(MiniProgramPage);
